<?php

namespace Pasoonate;

class Constants
{
	const J1970 = 2440587.5; // Julian date at Unix epoch: 1970-01-01
	const DayInSecond = 86400;
	const ShiaEpoch = 1948439.5;
	const JalaliEpoch = 1948320.5;
	const GregorianEpoch = 1721425.5;
	const IslamicEpoch = 1948439.5;
	const DaysOfIslamicYear = 354;
	const DaysOfShiaYear = 354;
	const DaysOfJalaliYear = 365;
	const DaysOfGregorianYear = 365;

    public function __construct()
    {

    }
}